﻿/*
 * Author: Cornelius Donley
 */

var locationName = null;
var numResults = 0;
var unit = 'F';
var geoLocFromGoogle = null;
var weatherData = null;
var historicWeatherData = null;

function getLocation() {
    var xhr = new XMLHttpRequest();
    var search = document.getElementById('input').value;
    var cleanSearch = removeDiacritics(search);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status == 200) {
            var result = JSON.parse(xhr.responseText);
            geoLocFromGoogle = result.results;
            fillLocations();
        }
    };
    xhr.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?&address=' + cleanSearch);
    xhr.send();
}

function fillLocations() {
    var str = '<form>';
    var i;
    if (geoLocFromGoogle.length == 1) {
        locationName = geoLocFromGoogle[0].formatted_address;
        getWeather(geoLocFromGoogle[0].geometry.location.lat, geoLocFromGoogle[0].geometry.location.lng, 0);
        numResults = 1;
    }
    else {
        numResults = 0;
        for (i = 0; i < geoLocFromGoogle.length; i++) {
            numResults++;
            var lat = geoLocFromGoogle[i].geometry.location.lat;
            var lng = geoLocFromGoogle[i].geometry.location.lng;
            var name = geoLocFromGoogle[i].formatted_address;
            str += "<input type='radio' name='location' value='" + name + "' onclick='getWeather(" + lat + ", " + lng + ", " + i + ")'>" + name + "<br/>";
        }
        str += '</form>';
    }
    document.getElementById('locations').innerHTML = str;
}

function saveSearch() {
    if (numResults == 0) return;
    if (typeof (Storage) !== "undefined") {
        if (localStorage.searchCount) {
            localStorage.searchCount = Number(localStorage.searchCount) + 1;
            localStorage.setItem(localStorage.searchCount, locationName);
        }
        else {
            localStorage.searchCount = 1;
            localStorage.setItem("1", locationName);
        }
        loadSearches();
    }
}

function selectSearch(event) {
    document.getElementById('input').value = event.target.value;
}

function loadSearches() {
    if (typeof (Storage) !== "undefined") {
        if (localStorage.searchCount) {
            var i;
            var str = "<option></option>";
            for (i = 1; i <= Number(localStorage.searchCount) ; i++) {
                str += '<option>';
                str += localStorage.getItem("" + i);
                str += '</option>';
            }
            document.getElementById('savedSearches').innerHTML = str;
        }
    }
}

function getWeather(lat, lng, pos) {
    var xhr = new XMLHttpRequest;
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status == 200) {
            weatherData = JSON.parse(xhr.responseText);
            locationName = geoLocFromGoogle[pos].formatted_address;
            displayWeather();
            document.getElementById('data').style.display = 'block';
            getOldWeather(lat, lng);
        }
    };
    xhr.open('GET', 'ForecastIO.ashx?lat=' + lat + '&lng=' + lng);
    xhr.send();
}

function getOldWeather(lat, lng) {
    var xhr = new XMLHttpRequest;
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status == 200) {
            historicWeatherData = JSON.parse(xhr.responseText);
            previousDayChart();
        }
    };
    xhr.open('GET', 'ForecastIO.ashx?lat=' + lat + '&lng=' + lng + '&time=' + (weatherData.currently.time - 86400));
    xhr.send();
}

function hourlyChart()
{
    var str = "Hour,Temperature\n";
    var i;
    var hourlyTemps = weatherData.hourly.data;
    for(i = 0; i < hourlyTemps.length; i++)
    {
        var time = moment.unix(hourlyTemps[i].time).format('YYYY-MM-DD hh:mm:ss a');
        if (unit == 'C') {
            str += time + "," + hourlyTemps[i].temperature.toFixed(2) + "\n";
        }
        else {
            str += time + "," + (hourlyTemps[i].temperature * 1.8 + 32).toFixed(1) + "\n";
        }
    }
    hg = new Dygraph(document.getElementById('hourlyChart'), str, { legend: 'always', title: '48-Hour Forecast', ylabel: 'Temperature', xlabel: 'Date'});
}

function dailyChart()
{
    var str = "Day,High,Low\n";
    var i;
    var dailyTemps = weatherData.daily.data;
    for (i = 0; i < dailyTemps.length; i++) {
        var time = moment.unix(dailyTemps[i].time).format('YYYY-MM-DD');
        if (unit == 'C') {
            str += time + "," + dailyTemps[i].temperatureMax.toFixed(2) + "," + dailyTemps[i].temperatureMin.toFixed(2) + "\n";
        }
        else {
            str += time + "," + (dailyTemps[i].temperatureMax * 1.8 + 32).toFixed(1) + "," + (dailyTemps[i].temperatureMin * 1.8 + 32).toFixed(1) + "\n";
        }
    }
    hg = new Dygraph(document.getElementById('dailyChart'), str, { legend: 'always', title: '7-Day Forecast', ylabel: 'Temperature', xlabel: 'Date' });
}

function previousDayChart()
{
    var str = "Hour,Temperature\n";
    var i;
    var hourlyTemps = historicWeatherData.hourly.data;
    for (i = 0; i < hourlyTemps.length; i++)
    {
        var time = moment.unix(hourlyTemps[i].time).format('YYYY-MM-DD hh:mm:ss a');
        if (unit == 'C')
        {
            str += time + "," + hourlyTemps[i].temperature.toFixed(2) + "\n";
        }
        else
        {
            str += time + "," + (hourlyTemps[i].temperature * 1.8 + 32).toFixed(1) + "\n";
        }
    }
    hg = new Dygraph(document.getElementById('oldHourlyChart'), str, { legend: 'always', title: 'Previous Day', ylabel: 'Temperature', xlabel: 'Date' });
}

function displayWeather() {
    document.getElementById('name').innerHTML = locationName;
    var currentTemp = document.getElementById('currentTemp');
    var humidity = document.getElementById('humidity');
    var cloudCover = document.getElementById('cloudCover');
    var precipProbability = document.getElementById('precipProbability');
    var visibility = document.getElementById('visibility');
    var summary = document.getElementById('summary');

    if (unit == 'C') {
        currentTemp.innerHTML = weatherData.currently.temperature.toFixed(2) + '&deg;C';
        visibility.innerHTML = weatherData.currently.visibility.toFixed(2) + 'km';
    }
    else {
        currentTemp.innerHTML = (weatherData.currently.temperature * 1.8 + 32).toFixed(1) + '&deg;F';
        visibility.innerHTML = (weatherData.currently.visibility * 0.62137).toFixed(2) + 'mi';
    }

    summary.innerHTML = weatherData.currently.summary;
    humidity.innerHTML = (weatherData.currently.humidity * 100).toFixed(0) + '%';
    cloudCover.innerHTML = (weatherData.currently.cloudCover * 100).toFixed(0) + '%';
    precipProbability.innerHTML = (weatherData.currently.precipProbability * 100).toFixed(0) + '%';

    hourlyChart();
    dailyChart();
}

function changeUnit(unit) {
    this.unit = unit;
    displayWeather();
    previousDayChart();
}

window.onload = loadSearches;