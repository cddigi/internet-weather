﻿/*
 * Author: Cornelius Donley
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using ForecastIO;
using ForecastIO.Extensions;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;

namespace SkyCast
{
    /// <summary>
    /// Summary description for ForecastIO
    /// </summary>
    public class ForecastIO : IHttpHandler
    {
        string API_KEY = "839f1c94f8f57a8ae636d12be96747ca";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            ForecastIOResponse response = new ForecastIOResponse();
            if(context.Request.HttpMethod.Equals("GET"))
            {
                var latitude = float.Parse(context.Request.Params["lat"]);
                var longitude = float.Parse(context.Request.Params["lng"]);
                var unixTime = context.Request.Params["time"];               
                ForecastIORequest request;
                if (unixTime == null)
                    request = new ForecastIORequest(API_KEY, latitude, longitude, Unit.si);
                else
                {
                    var time = long.Parse(unixTime).ToDateTime();
                    request = new ForecastIORequest(API_KEY, latitude, longitude, time, Unit.si);
                }
                response = request.Get();
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(response);
                context.Response.Write(json.ToString());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}